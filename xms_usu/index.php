<?php include("include/header.php")?>
    <?php 

    require_once "config/config.php";
    require_once "core/routes.php";
    require_once "config/dbConexion.php";
    require_once "controller/Main.php";

    
    $controller=new MainController();
    //$controller->index();
    if(isset($_GET['c'])){
        
        $controlador=$_GET['c'];
        $controlador = loadController($_GET['c']);	

        if(isset($_GET['a'])){
            loadAction($controlador, $_GET['a']);

        }
        else{
            loadAction($controlador,INDEX);
        }

	}else{
        $controlador = loadController(MAIN_CONTROLLER);
        $accionTmp = INDEX;
        $controlador->$accionTmp();
    }
    
    ?>
 

<?php include("include/footer.php")?>