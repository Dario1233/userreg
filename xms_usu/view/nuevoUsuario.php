<?php require_once "include/header.php";?>
<main class="container p-3">
    
    <div class="row">
        <div class="col-md-3">
        <div class="card card-header">
            <h4> Nuevo usuario</h4>
        </div>
            <div class=" card card-body">
                <form  method="POST" autocomplete="off" action="index.php?c=Main&a=add">
                    <div class="form-group">
                        <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Nombre" required minlength="2">
                    </div>
                    <div class="form-group">
                        <input type="text" id="apellido" name="apellido" class="form-control" placeholder="Apellido" required minlength="2">
                    </div>
                    <div class="form-group">
                        <input type="text" name="rut" id="rut" class="form-control" placeholder="Rut ej:12345678-9" required minlength="9" maxlength="10"  pattern="\d{3,8}-[\d|kK]{1}">
                    </div>
                    <div class="form-group">
                        <input type="text" name="direccion" id="direccion" class="form-control" placeholder="Direccion" required minlength="2">
                    </div>
                    <div class="form-group">
                        <input type="text" name="cargo" id="cargo" class="form-control" placeholder="Cargo" required minlength="2">
                    </div>
                    <input type="submit" class="btn btn-success">
                </form>
            </div>
        </div>
    
        <div class="col-md-9">
            <table class="table table-striped table-bordered">
            <thead class="thead-dark">
                <tr>
                <th scope="col">id</th>
                <th scope="col">Nombre</th>
                <th scope="col">Apellido</th>
                <th scope="col">Rut</th>
                <th scope="col">Direccion</th>
                <th scope="col">Cargo</th>
                <th scope="col">Fecha Ingreso</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach($data["usuarios"] as $dato){
                    echo "<tr>";
                    echo "<td>".$dato["id"]."</td>";
                    echo "<td>".$dato["nombre"]."</td>";
                    echo "<td>".$dato["apellido"]."</td>";
                    echo "<td>".$dato["rut"]."</td>";
                    echo "<td>".$dato["direccion"]."</td>";
                    echo "<td>".$dato["cargo"]."</td>";
                    echo "<td>".$dato["fecha_ingreso"]."</td>";
                    echo "</tr>";

                } ?>
            </tbody>

            </table>
        </div>
    
    </div>
</main>
<script type="text/javascript">

var nom = document.getElementById("nombre");
nom.addEventListener("blur", funcionBlur, true);

var ap = document.getElementById("apellido");
ap.addEventListener("blur", funcionBlur, true);

function funcionBlur() {
    var nombre = document.getElementById("nombre");
    var apellido=document.getElementById("apellido");
    nombre.value = capitalize(nombre.value);
    apellido.value = capitalize(apellido.value);
}



function capitalize(s){
    return s.toLowerCase().replace( /\b./g, function(a){ return a.toUpperCase(); } );
}

</script>

<?php require_once "include/footer.php";?>